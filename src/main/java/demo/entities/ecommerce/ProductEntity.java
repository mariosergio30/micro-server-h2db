package demo.entities.ecommerce;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_ECOM_PRODUCT")  
public class ProductEntity {  

	@Id
	@Column(name="code", nullable=false, unique=false)  
	private String code;

	@Column(name="name", nullable=false)
	private String name;

	private String especification;

	private String category;

	@Column(name="sale_price", nullable=false)
	private Double salePrice;

	@ManyToOne
	@JoinColumn(name = "supplier_id")      
	private SupplierEntity supplier;

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEspecification() {
		return especification;
	}

	public void setEspecification(String especification) {
		this.especification = especification;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	public SupplierEntity getSupplier() {
		return supplier;
	}

	public void setSupplier(SupplierEntity supplier) {
		this.supplier = supplier;
	}





}